data "aws_caller_identity" "account" {}

data "aws_availability_zones" "available" {}

data "cloudinit_config" "nat-gateway-config" {
  count         = var.enable_nat_instance == true ? 1 : 0
  gzip          = false
  base64_encode = true

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = templatefile(fileexists("templates/nat_gateway.cloud_config") ? "templates/nat_gateway.cloud_config" : "${path.module}/files/nat_gateway.cloud_config.tftpl", { hostname = "${local.prefix}nat-gateway" })
  }

  part {
    filename     = "init.sh"
    content_type = "text/x-shellscript"
    content      = fileexists("templates/nat_script.cloud_config") ? file("templates/nat_script.cloud_config") : file("${path.module}/files/nat_script.cloud_config.tftpl")
  }
}

data "aws_instances" "NatCreated" {
  count = var.enable_nat_instance == true ? 1 : 0
  filter {
    name   = "tag:Name"
    values = ["${local.prefix}nat-gateway"]
  }

  filter {
    name   = "tag:APP"
    values = ["Nat-gateway"]
  }

  depends_on = [aws_autoscaling_group.this]
}

data "aws_instance" "NatCreatedASG" {
  count       = var.enable_nat_instance == true ? 1 : 0
  instance_id = data.aws_instances.NatCreated[0].ids[0]

  depends_on = [aws_autoscaling_group.this]
}